all:  wordblah wordblah_player

wordblah: wordblah.c wordblah.h constantstrings.h
	clang wordblah.c -o wordblah -lgd -lz -lcrypto
	 
wordblah_player: wordblah_player.c wordblah.h wordblah.gresource.xml wordblah_player.glade constantstrings.h
		glib-compile-resources wordblah.gresource.xml --target wordblah_resource.c --generate-source
		clang -rdynamic -o wordblah_player wordblah_player.c -Wall `pkg-config --cflags --libs gtk+-3.0` -lgd -lz -lcrypto
