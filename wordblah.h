#ifndef __WORDBLAH_H
#define __WORDBLAH_H
#define _XOPEN_SOURCE
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <gd.h>
#include <gdfontmb.h>
#include <gdfontg.h>
#include <zlib.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include "constantstrings.h"

#define MAX_PUZZLE_SIZE 25
#define MAX_CLUE_LENGTH 150
#define GRID_PIXELS 37

/* Enum to define terminal colours */
enum COLOR {
	BLACK = 0,
	RED= 1,
	GREEN=2,
	YELLOW=3,
	BLUE=4,
	MAGENTA=5,
	CYAN=6,
	WHITE=7
};

/* Enum to define terminal attributes */
enum ATTR {
	NORMAL = 23,
	BOLD=1
};

/* Enum to describe current movement orientation in puzzle grid */
enum ORIENTATION {
	ACROSS=1,
	DOWN=2
};

/* for use with the player */
enum DIRECTION {
	DIR_FORWARD = 1,
	DIR_BACK = -1
};

typedef char String[MAX_CLUE_LENGTH];

/* The main puzzle struct type */
typedef struct {
	char chars[MAX_PUZZLE_SIZE+1][MAX_PUZZLE_SIZE+1];
	int start_across_word[MAX_PUZZLE_SIZE][MAX_PUZZLE_SIZE];
	int start_down_word[MAX_PUZZLE_SIZE][MAX_PUZZLE_SIZE];
	String clue_across[MAX_PUZZLE_SIZE][MAX_PUZZLE_SIZE];
	String clue_down[MAX_PUZZLE_SIZE][MAX_PUZZLE_SIZE];
	int grid_size;
	bool grid_frozen;
	char hashed_master_password[256];
	char hashed_solution_password[256];
} Puzzle;

/* The player data struct type - for the player app */
typedef struct {
	Puzzle puzzle;
	char filename[65535];
	bool is_loaded;
	char char_ans[MAX_PUZZLE_SIZE+1][MAX_PUZZLE_SIZE+1];
	int cur_row;
	int cur_col;
	bool solution_revealed;
	enum ORIENTATION current_movement;
} MainPlayerData;

/* compute the hash of a password */
void digest_message(const unsigned char *message, 
	size_t message_len, unsigned char **digest, unsigned int *digest_len)
{
	EVP_MD_CTX *mdctx;

	if((mdctx = EVP_MD_CTX_new()) == NULL)
		goto err;

	if(1 != EVP_DigestInit_ex(mdctx, EVP_sha256(), NULL))
		goto err;

	if(1 != EVP_DigestUpdate(mdctx, message, message_len))
		goto err;

	if((*digest = (unsigned char *)
		OPENSSL_malloc(EVP_MD_size(EVP_sha256()))) == NULL)
		goto err;

	if(1 != EVP_DigestFinal_ex(mdctx, *digest, digest_len))
		goto err;

	EVP_MD_CTX_free(mdctx);
	return;
err:
	EVP_MD_CTX_free(mdctx);
	ERR_print_errors_fp(stderr);	
	exit (2);
}

/* encode the binary data to readable text format using OpenSSL - also call
   OPENSSL_free if the binary data was allocated by OpenSSL */
void encode_binary (char *encoded, unsigned char *binary_data, unsigned int len,
						bool free_openssl_data)
{

	EVP_EncodeBlock ((unsigned char*)encoded, 
							(const unsigned char*)binary_data, len);
	if (free_openssl_data)
		OPENSSL_free (binary_data);
}

/* decode the binary data from the textual representation using OpenSSL */
void decode_binary (char *bin_data, char *encoded)
{
	EVP_DecodeBlock ((unsigned char*)bin_data, 
							(const unsigned char*)encoded, strlen (encoded));
}

/* encrypt a block of text using password/passphrase with OpenSSL and 
   also encode it to textual representation */
void encrypt_data (char *enc_data, const char *data, const char *password)
{
	EVP_CIPHER_CTX *ctx;
	int len, cipher_len; 
	ctx = EVP_CIPHER_CTX_new ();
	if (! ctx) goto err;
	
	unsigned char encrypted[256] = { '\0' };
	
	unsigned char key[EVP_MAX_KEY_LENGTH] = { '\0'};
	unsigned char iv[EVP_MAX_IV_LENGTH] = { '\0' };
	
	if (! EVP_BytesToKey (EVP_aes_256_cbc(), EVP_md5(), NULL, 
									(unsigned char*)password, strlen(password),
										10, key, iv))
		goto err;
	
	if (1 != EVP_EncryptInit_ex (ctx, EVP_aes_256_cbc(), NULL, key, iv))
		goto err;
	
	if (1 != EVP_EncryptUpdate (ctx, (unsigned char*) encrypted, &len, 
								(unsigned char*) data, strlen (data) ))
		goto err;
	cipher_len = len;
	
	if (1 != EVP_EncryptFinal_ex (ctx, encrypted + len, &len))
		goto err;
	
	cipher_len += len;
	EVP_CIPHER_CTX_free (ctx);
	
	EVP_EncodeBlock ((unsigned char*) enc_data, (unsigned char*) encrypted, 
						cipher_len);
	return;
 err:
 	ERR_print_errors_fp (stderr);
 	EVP_CIPHER_CTX_free (ctx);
 	exit (2);
}

/* decrypt a block of text using password/passphrase with OpenSSL */
void decrypt_data (char *dec_data, const char *data, const char *password)
{
	EVP_CIPHER_CTX *ctx;
	int len, text_len; 
	ctx = EVP_CIPHER_CTX_new ();
	if (! ctx) goto err;
	
	char enc_data[256] = { '\0' };
	
	unsigned char key[EVP_MAX_KEY_LENGTH] = { '\0'};
	unsigned char iv[EVP_MAX_IV_LENGTH] = { '\0' };

	
	if (! EVP_BytesToKey (EVP_aes_256_cbc(), EVP_md5(), NULL, 
									(unsigned char*)password, strlen(password),
										10, key, iv))
		goto err;
	
	int r = EVP_DecodeBlock ((unsigned char*)enc_data, 
								(const unsigned char*) data, strlen (data));
	if (-1 == r)
		goto err;
	
	
	if (1 != EVP_DecryptInit_ex (ctx, EVP_aes_256_cbc(), NULL, key, iv))
		goto err;
	
	if (1 != EVP_DecryptUpdate (ctx, (unsigned char*) dec_data, &len, 
								(unsigned char*) enc_data, r - (r % 16) ))
		goto err;
	text_len = len;
	
	if (1 != EVP_DecryptFinal_ex (ctx, (unsigned char *)dec_data + len, &len))
		goto err;
	
	text_len += len;
	EVP_CIPHER_CTX_free (ctx);
	
	dec_data[text_len] = '\0';
		
	return;
 err:
 	ERR_print_errors_fp (stderr);
 	EVP_CIPHER_CTX_free (ctx);
 	exit (2);
}

/* get a number from the user */
int get_num ()
{
	char s[5];
	fgets (s, 5, stdin);
	int n = atoi (s);
	return n;
}

/* verify solution password */
bool verify_solution_password (Puzzle *p, const char* password)
{
	/* no password set */
	if (strcmp (p->hashed_solution_password, "\0") == 0)
		return true;
		
	/* hash the user input password and compare it with the stored password */
	unsigned char* hashed_sol_password;
	unsigned int len;
	digest_message ((const unsigned char *)password, strlen(password),
						&hashed_sol_password, &len);
	char hashed_hex_pwd[256] = { '\0' };
	encode_binary (hashed_hex_pwd, hashed_sol_password, len, true);
	
	if (strcmp (p->hashed_solution_password, hashed_hex_pwd) == 0)
		return true;
	
	return false;
}


/* verify master password */
bool verify_master_password (Puzzle *p, const char* password)
{
	/* no password set */
	if (strcmp (p->hashed_master_password, "\0") == 0)
		return true;
		
	/* hash the user input password and compare it with the stored password */
	unsigned char* hashed_mas_password;
	unsigned int len;
	digest_message ((const unsigned char *)password, strlen(password),
						&hashed_mas_password, &len);
	char hashed_hex_pwd[256] = { '\0' };
	encode_binary (hashed_hex_pwd, hashed_mas_password, len, true);
	
	if (strcmp (p->hashed_master_password, hashed_hex_pwd) == 0)
		return true;
	
	return false;
}

/* Set or reset solution password for puzzle */
void set_solution_password (Puzzle *p, const char *password)
{
	/* if it is a null string, reset the password */
	if (strcmp (password, "\0") == 0)
		strcpy (p->hashed_solution_password, "\0");
	else 
	{

		unsigned char* hashedpwd;
		unsigned int len;
		digest_message ((const unsigned char *)password, strlen(password),
						&hashedpwd, &len);
		/* the hashedpwd contains binary data - we will convert it to 
		   hexadecimal data and store in file */

		encode_binary (p->hashed_solution_password, hashedpwd, len, true);
	}
}

/* Set or reset master password for puzzle */
void set_master_password (Puzzle *p, const char *password)
{
	/* if it is a null string, reset the password */
	if (strcmp (password, "\0") == 0)
		strcpy (p->hashed_master_password, "\0");
	else 
	{

		unsigned char* hashedpwd;
		unsigned int len;
		digest_message ((const unsigned char *)password, strlen(password),
						&hashedpwd, &len);
		/* the hashedpwd contains binary data - we will convert it to 
		   hexadecimal data and store in file */

		encode_binary (p->hashed_master_password, hashedpwd, len, true);
	}
}

/* Output the clues to text file */
void export_clues (Puzzle *p, const char *filename)
{
	FILE *outfile = fopen (filename, "w");
	if (outfile == NULL)
	{
		fprintf (stderr, "%s\n", ERROR_WRITING_FILE);
		exit (1);
	}
	/* first the across clues */
	fprintf (outfile, "ACROSS CLUES\n");
	for (int i = 0; i < p->grid_size; i ++)
	{
		for (int j = 0; j < p->grid_size; j ++)
		{
			if (p->start_across_word[i][j] != -1)
				fprintf (outfile, "%d - %s\n", p->start_across_word[i][j],
							p->clue_across[i][j]);
		}
	}
	/* now the down clues */
	fprintf (outfile, "DOWN CLUES\n");
	for (int i = 0; i < p->grid_size; i ++)
	{
		for (int j = 0; j < p->grid_size; j ++)
		{
			if (p->start_down_word[i][j] != -1)
				fprintf (outfile, "%d - %s\n", p->start_down_word[i][j],
							p->clue_down[i][j]);
		}
	}
	fclose (outfile);
}

/* Output the grid to image - if answerkey is true export filled grid */
void export_grid_image  (Puzzle *p, const char *filename, bool answerkey) 
{
	int img_size = p->grid_size * GRID_PIXELS;
	FILE * outfile = fopen (filename, "wb");
	if (outfile == NULL)
	{
		fprintf (stderr, "%s\n", ERROR_WRITING_FILE);
		exit (1);
	}
	
	gdImagePtr img = gdImageCreate (img_size, img_size);
	gdImageColorAllocate (img, 255,255,255);
	int black = gdImageColorAllocate (img, 0, 0, 0);
	int blue = gdImageColorAllocate (img, 0, 0, 216);
	gdFontPtr sm_fnt = gdFontGetMediumBold ();
	gdFontPtr lg_fnt = gdFontGetGiant ();
	
	for (int i = 0; i < p->grid_size; i ++)
	{
		for (int j = 0; j < p->grid_size; j++)
		{
			/* if it is a block, draw the black square */
			if (p->chars[i][j] == '#')
				gdImageFilledRectangle (img, j*GRID_PIXELS, i*GRID_PIXELS, 
										j*GRID_PIXELS+GRID_PIXELS, 
										i*GRID_PIXELS+GRID_PIXELS,black);
			else
			{
				/* draw a regular square */
				gdImageRectangle (img, j*GRID_PIXELS, i*GRID_PIXELS, 
										j*GRID_PIXELS+GRID_PIXELS, 
									i*GRID_PIXELS+GRID_PIXELS, black);
				
				/* print the numers, if it is either start across word or 
				a down word */
				if (p->start_across_word[i][j] != -1 || 
						p->start_down_word[i][j] != -1)
				{
					if (p->start_across_word[i][j] != -1)
					{
						char str[5];
						sprintf (str, "%d", p->start_across_word[i][j]);
						gdImageString (img,  sm_fnt, j*GRID_PIXELS+2, 
										i*GRID_PIXELS+2, 
										(unsigned char *)str, blue);   
					}
					else 
					{
						char str[5];
						sprintf (str, "%d", p->start_down_word[i][j]);
						gdImageString (img,  sm_fnt, j*GRID_PIXELS+2, 
										i*GRID_PIXELS+2,
										(unsigned char *)str, blue);					
					}
				}
				/* if answerkey is true, draw the character in the cell */
				if (answerkey)
				{
					gdImageChar (img, lg_fnt, j*GRID_PIXELS+15, 
									i*GRID_PIXELS+10, p->chars[i][j], black);
				}
			}
		}
	}
	
	gdImagePng (img, outfile);
	gdImageDestroy (img);
	fclose (outfile);
}

/* Set the terminal colour */
void set_color (enum COLOR fg, enum COLOR bg, enum ATTR at) {
	printf ("\x1B[%d;%d;%dm", fg+30, bg+40, at);
}

/* Reset the terminal colour */
void reset_color () {
	printf ("\x1B[0m");
}

/* check if the prev row has a block or not */
bool prev_row_block (Puzzle *p, int r, int c)
{
	if (r == 0)
		return true;
	if (p->chars[r-1][c] == '#')
		return true;
	return false;
}

/* check if the next row has a block or not */
bool next_row_block (Puzzle *p, int r, int c)
{
	if (r == p->grid_size-1)
		return true;
	if (p->chars[r+1][c] == '#')
		return true;
	return false;
}

/* check if the prev col has a block or not */
bool prev_col_block (Puzzle *p, int r, int c)
{
	if (c == 0)
		return true;
	if (p->chars[r][c-1] == '#')
		return true;
	return false;
}

/* check if the next col has a block or not */
bool next_col_block (Puzzle *p, int r, int c)
{
	if (c == p->grid_size - 1)
		return true;
	if (p->chars[r][c+1] == '#')
		return true;
	return false;
}

/* check if previous row is blank or not */
bool prev_row_blank (Puzzle *p, int r, int c) 
{
	if (r == 0) return true;
	if (p->chars[r-1][c] == ' ' || p->chars[r-1][c] == '#') return true;
	return false;
}
/* check if next row is blank or not */
bool next_row_blank (Puzzle *p, int r, int c)
{
	if (r == p->grid_size - 1) return true;
	if (p->chars[r+1][c] == ' ' || p->chars[r+1][c] == '#') return true;
	return false;
}
/* check if previous col is blank or not */
bool prev_col_blank (Puzzle *p, int r, int c)
{
	if (c == 0) return true;
	if (p->chars[r][c-1] == ' ' || p->chars[r][c-1] == '#') return true;
	return false; 
}
/* check if the next col is blank or not */
bool next_col_blank (Puzzle *p, int r, int c)
{
	if (c == p->grid_size -1) return true;
	if (p->chars[r][c+1] == ' ' || p->chars[r][c+1] == '#') return true;
	return false;
}

/* set the current row/col to the beginning of word index (across or down) */
void set_selection_to_word_start (MainPlayerData *app_data,
								 enum ORIENTATION orient, int word_index)
{
	for (int i = 0; i < app_data->puzzle.grid_size; i ++)
	{
		for (int j = 0; j < app_data->puzzle.grid_size; j ++)
		{
			if (orient == ACROSS && 
				app_data->puzzle.start_across_word[i][j] == word_index)
			{
				app_data->current_movement = ACROSS;
				app_data->cur_row = i;
				app_data->cur_col = j;
				break;
			}
			else if (orient == DOWN && 
				app_data->puzzle.start_down_word[i][j] == word_index)
			{
				app_data->current_movement = DOWN;
				app_data->cur_row = i;
				app_data->cur_col = j;
				break;
			}
		}
	}
}

/* unfreeze the grid - make editing possible to change words */
void unfreeze_puzzle (Puzzle *p)
{
	for (int i = 0; i < p->grid_size; i ++)
	{
		for (int j = 0; j < p->grid_size; j ++)
		{
			if (p->chars[i][j] == '#')
				p->chars[i][j] = ' ';
			
			p->start_across_word[i][j] = -1;
			p->start_down_word[i][j] = -1;
		}
	}
	p->grid_frozen = false;
}

/* freeze the grid - make editing impossible because it finalizes the
   across and down words in the grid */
void freeze_puzzle (Puzzle *p)
{		
	int word_num = 1;
	bool across_word_start, down_word_start;
	for (int i = 0; i < p->grid_size; i ++)
	{
		for (int j = 0; j < p->grid_size; j++)
		{
			across_word_start = false; 
			down_word_start = false;
			/* if it is a blank cell - cover it with a block */
			if (p->chars[i][j] == ' ' || p->chars[i][j] == '#')
				p->chars[i][j] = '#';
			/* it is not a blank cell - check all possibilities */
			else 
			{
				bool prev_row = prev_row_blank (p, i, j);
				bool next_row = next_row_blank (p, i, j);
				bool prev_col = prev_col_blank (p, i, j);
				bool next_col = next_col_blank (p, i, j);
				if (prev_row && ! next_row)
					down_word_start = true;
				if (prev_col && ! next_col)
					across_word_start = true;
			}
			
			if (across_word_start == true)
				p->start_across_word[i][j] = word_num;
			else 
				p->start_across_word[i][j] = -1;
			if (down_word_start == true)
				p->start_down_word[i][j] = word_num;
			else
				p->start_down_word[i][j] = -1;
			if (across_word_start == true || down_word_start == true)
				word_num ++;
		}
	}
	p->grid_frozen = true;
}

/* reset the entire grid */
void init_puzzle (Puzzle *p, int grid_size) 
{
	/* check for bounds */
	if (p->grid_size > MAX_PUZZLE_SIZE)
		p->grid_size = MAX_PUZZLE_SIZE;
	else
		p->grid_size = grid_size;
		
	/* grid is always unfrozen for a new puzzle */
	p->grid_frozen = false;
	
	/* initialize all the puzzle data - characters, start of words (across/down)
	   and the clues to null */
	for (int i = 0; i < p->grid_size; i ++)
	{
		for (int j = 0; j < p->grid_size; j ++) 
		{
			p->chars[i][j] = ' ';
			p->start_across_word[i][j] = -1;
			p->start_down_word[i][j] = -1;
			strcpy (p->clue_across[i][j], "\0");
			strcpy (p->clue_down[i][j], "\0");
		}
	}
	/* reset the master password and solution password */
	strcpy (p->hashed_master_password, "\0");
	strcpy (p->hashed_solution_password, "\0");
	
}

/* save the puzzle to a file */
void save_puzzle (Puzzle *puzzle, const char* file) 
{
	FILE *outfile;
	/* First output the uncompressed contents to a temp file */
	outfile = tmpfile ();
	if (outfile == NULL)
	{
		fprintf (stderr, "%s\n", ERROR_WRITING_FILE);
		exit (1);
	}
	/* grid size is the first field */
	fprintf (outfile, "%d\n", puzzle->grid_size);
	/* whether grid is frozen or not */
	fprintf (outfile, "%d\n", puzzle->grid_frozen);
	/* the hashed password */
	fprintf (outfile, "%s\n", puzzle->hashed_master_password);
	/* the hashed_solution_password */
	fprintf (outfile, "%s\n", puzzle->hashed_solution_password);
	
	/* First output the grid characters columns/rows as encrypted */
	for (int i = 0; i < puzzle->grid_size; i ++)
	{
		char encrypted[256] = { '\0' };
		/* encrypt the grid characters at row i with master password to
		   generate the key and iv */
		encrypt_data (encrypted, puzzle->chars[i], 
								puzzle->hashed_master_password);

		fprintf (outfile, "%s", encrypted);
		fprintf (outfile, "\n");
	}
	
	/* Next output the start across/down numbers */
	for (int i = 0; i < puzzle->grid_size; i ++)
	{
		for (int j = 0; j < puzzle->grid_size; j++)
		{
			fprintf (outfile, "%d ", puzzle->start_across_word[i][j]);
			fprintf (outfile, "%d ", puzzle->start_down_word[i][j]);
		}
		fprintf (outfile, "\n");
	}
	
	/* Output the across clues */
	fprintf (outfile, "ACROSS\n");
	/* Search the grid for across words */
	for (int i = 0; i < puzzle->grid_size; i ++)
	{
		for (int j = 0; j < puzzle->grid_size; j++)
		{
			/* if it is an across word, then put the word index followed by
			tab character (as separator) and the clue */
			if (puzzle->start_across_word[i][j] != -1)
				fprintf (outfile, "%d\t%s\n", puzzle->start_across_word[i][j], 
					puzzle->clue_across[i][j]);
		}
	}
	
	/* Output the down clues */
	fprintf (outfile, "DOWN\n");
	/* Search the grid for down words */
	for (int i = 0; i < puzzle->grid_size; i ++)
	{
		for (int j = 0; j < puzzle->grid_size; j++)
		{
			/* same as across word, put the word index followed by the tab
			character and then the clue */
			if (puzzle->start_down_word[i][j] != -1)
				fprintf (outfile, "%d\t%s\n", puzzle->start_down_word[i][j], 
					puzzle->clue_down[i][j]);
		}
	}
	
	/* Flush the buffer and rewind to beginning - to read and save into 
	gzip compressed file */
	fflush (outfile);
	fseek (outfile, 0, 0);
			
	/* now compress the file and save it to destination file */
	gzFile outdestfile = gzopen (file, "wb");
	if (outdestfile == NULL)
	{
		fprintf (stderr, "%s\n", ERROR_WRITING_FILE);
		fclose (outfile);
		exit (1);
	}
	char buf[128];
	int num = fread (buf, sizeof(char), sizeof(char)*128, outfile);
	while (num > 0)
	{
		int res = gzwrite (outdestfile, buf, num*sizeof(char) );
		if (res == 0)
		{
			fprintf (stderr, "%s %s\n", ERROR_WRITING_FILE, COMPRESSED);
			fclose (outfile);
			exit (1);
		}
		num = fread (buf, sizeof(char), sizeof(char)*128, outfile);
	}
	gzclose (outdestfile);
	fclose (outfile);

}

/* read the puzzle from a file */
Puzzle load_puzzle (const char* file) 
{
	Puzzle p;
	/* First open the GZip file */
	gzFile insourcefile = gzopen (file, "rb");
	if (insourcefile == NULL)
	{
		fprintf (stderr, "%s %s\n", ERROR_READING_FILE, COMPRESSED);
		/* return an invalid puzzle */
		init_puzzle (&p, 0);
		return p;
	}
	/* Open a temporary file to uncompress the contents */
	FILE *infile = tmpfile ();
	if (infile == NULL)
	{
		fprintf (stderr, "%s\n", ERROR_READING_FILE);
		init_puzzle (&p, 0);
		return p;
	}
	/* Put the uncompressed content to the temp file */
	char buf[128];
	int num = 0;
	num = gzread (insourcefile, buf, 128);
	while (num > 0)
	{
		int res = fwrite (buf, 1, num, infile);
		if (res == 0)
		{
			fprintf (stderr, "%s\n", ERROR_READING_FILE);
			fclose (infile);
			gzclose (insourcefile);
			/* return an invalid puzzle */
			init_puzzle (&p, 0);
			return p;
		}
		num = gzread (insourcefile, buf, 128);
	}
	/* Close the gzip file */
	gzclose (insourcefile);
	/* Flush the temp file buffer and rewind to beginning */
	fflush (infile);
	fseek (infile, 0, 0);

	/* Read the temporary file contents to the structure Puzzle */
	char line[MAX_CLUE_LENGTH+10];
	fgets (line, MAX_CLUE_LENGTH + 10, infile);
	p.grid_size = atoi (line);
	/* if puzzle is invalid or otherwise not proper grid, return an invalid
	   puzzle object */
	if (p.grid_size == 0)
	{
		fprintf (stderr, "%s\n", INVALID_PUZZLE);
		init_puzzle (&p, 0);
		return p;
	}
	fgets (line, MAX_CLUE_LENGTH + 10, infile);
	p.grid_frozen = atoi (line) == 0 ? false : true ;
	fgets (line, MAX_CLUE_LENGTH + 10, infile);
	if (strlen (line) != 1)
		strcpy (p.hashed_master_password, strtok (line, "\n"));
	else
		strcpy (p.hashed_master_password, "\0");
	fgets (line, MAX_CLUE_LENGTH + 10, infile);
	if (strlen (line) != 1)
		strcpy (p.hashed_solution_password, strtok (line, "\n"));
	else
		strcpy (p.hashed_solution_password, "\0");
	
	/* read each character of the grid */
	for (int i = 0; i < p.grid_size; i ++ )
	{
		char encrypted[256];
		/* get a line from the file - each line is a grid row */
		fgets (encrypted, MAX_CLUE_LENGTH + 10, infile);
		/* decrypt each line from the file and put the decrypted chars 
		   into the grid array */
		decrypt_data (line, encrypted, p.hashed_master_password);
		/* finally read the decrypted data into the array */
		for (int j = 0; j < p.grid_size; j ++)
			p.chars[i][j] = line[j];

	}
	/* read the word numbers */
	for (int i = 0; i < p.grid_size; i ++)
	{
		/* get a line from the file - each file represents a row */
		/* the word numbers are started as n1<space>n2 where n1 is 
		   the across word number and n2 is the down word number. 
		   Though both across and down word numbers will be the same
		   in a given cell, we use separate number to determine whether
		   there is an across or down word or both in a given cell. */
		fgets (line, MAX_CLUE_LENGTH + 10, infile);
		/* split the line into tokens with space as the separating character */
		char *token = strtok (line, " ");
		for (int j = 0; j < p.grid_size; j ++)
		{
			/* so long as token is valid, read the first token as across 
			   word number */
			if (token != NULL) 
				p.start_across_word[i][j] = atoi (token);
			/* similarly get the next token as the down word number */
			token = strtok (NULL, " ");
			if (token != NULL)
				p.start_down_word[i][j] = atoi (token);
			/* get the next token, it should be the across word number format
			   the next cell or NULL if we have read all the tokens */
			token = strtok (NULL, " ");
		}
	}
	/* read the clues */
	fgets (line, MAX_CLUE_LENGTH + 10, infile);
	
	/* across clues */
	char clues[100][MAX_CLUE_LENGTH];
	int word_num[100];
	int c = 0;
	/* first read the across clues from file */
	while (1)
	{
		fgets (line, MAX_CLUE_LENGTH + 10, infile);
		/* the word DOWN indicates that we reached the end of across clues */
		if (strcmp (line, "DOWN\n") == 0)
			break;
		word_num[c] = atoi (strtok (line, "\t"));
		char *cl = strtok (NULL, "\n");
		if (cl != NULL)		
			strcpy (clues[c], cl);
		else
			strcpy (clues[c], "\0");
		c++;
	}
	/* set the clue to the correct cell in grid */
	for (int i = 0; i < p.grid_size; i ++)
	{
		for (int j = 0; j < p.grid_size; j ++)
		{
			for (int r = 0; r < c; r ++)
				if (p.start_across_word[i][j] == word_num[r])
					strcpy (p.clue_across[i][j], clues[r]);
		}
	}

	/* down clues */
	c = 0;
	while (fgets (line, MAX_CLUE_LENGTH + 10, infile))
	{
		word_num[c] = atoi (strtok (line, "\t"));
		char* cl = strtok (NULL, "\n");
		if (cl != NULL)
			strcpy (clues[c], cl);
		else
			strcpy (clues[c], "\0");
		c++;
	}
	for (int i = 0; i < p.grid_size; i ++)
	{
		for (int j = 0; j < p.grid_size; j ++)
		{
			for (int r = 0; r < c; r ++)
				if (p.start_down_word[i][j] == word_num[r])
					strcpy (p.clue_down[i][j], clues[r]);
		}
	}

	fclose (infile);
	return p;
}

/* display the puzzle */ 
void print_puzzle (Puzzle *p) 
{
	printf ("\n");
	set_color (WHITE, CYAN, NORMAL);
	printf ("    ");
	for (int i = 0; i < p->grid_size; i ++)
		printf ("%3d", i);
	reset_color ();
	printf("\n");
	for (int i = 0; i < p->grid_size; i ++)
	{
		set_color (WHITE, CYAN, NORMAL);
		printf ("%3d ", i);
		for (int j = 0; j < p->grid_size; j ++) 
		{
			if (p->chars[i][j] == '#') {
				set_color (WHITE, BLACK, NORMAL);
				printf ("   ");
			}
			else 
			{
				if (p->start_across_word[i][j] != -1 || 
					p->start_down_word[i][j] != -1) 
				{
					set_color (BLUE, WHITE, NORMAL);
					if (p->start_across_word[i][j] != -1)
						printf ("%-2d", p->start_across_word[i][j]);
					else 
						printf ("%-2d", p->start_down_word[i][j]);
				}
				else
				{
					set_color (BLACK, WHITE,NORMAL);
					printf ("  ");
				}
					
				set_color (BLACK, WHITE, BOLD);
				printf ("%c", p->chars[i][j]);
			}
			reset_color ();
		}
		printf ("\n");
	}
	/* print the clues if set */
	if (p->grid_frozen == true) 
	{
		printf ("\x1B[1m%s\x1B[0m\n", ACROSS_CLUES);
		for (int i = 0; i < p->grid_size; i ++)
		{
			for (int j = 0; j < p->grid_size; j ++)
			{
				if (p->start_across_word[i][j] != -1)
				{
					printf ("%d - %s; ", p->start_across_word[i][j], 
								p->clue_across[i][j]);
				}
			}
		}
		printf ("\n\x1B[1m%s\x1B[0m\n", DOWN_CLUES);
		for (int i = 0; i < p->grid_size; i ++)
		{
			for (int j = 0; j < p->grid_size; j ++)
			{
				if (p->start_down_word[i][j] != -1)
				{
					printf ("%d - %s; ", p->start_down_word[i][j], 
								p->clue_down[i][j]);
				}
			}
		}
		printf ("\n");	
	}
}

/* function to check if a word is valid or not */
char* is_valid_word (char *word) 
{
	if (word == NULL || strlen(word) == 0)
		return NULL;
	for (int i = 0; i < strlen (word) - 1; i ++)
		if (! isalpha (word[i])) 
			return NULL;
			
	return strtok (word, "\n");
}


/* function to set a clue for an across word */
bool set_clue (Puzzle *p, String clue, int index, enum ORIENTATION order) 
{
	for (int i = 0; i < p->grid_size; i ++)
	{
		for (int j = 0; j < p->grid_size; j ++)
		{
			if (order == ACROSS)
			{
				if (p->start_across_word[i][j] == index)
				{
					strcpy (p->clue_across[i][j], clue);
					return true;
				}
			}
			else if (order == DOWN)
			{
				if (p->start_down_word[i][j] == index)
				{
					strcpy (p->clue_down[i][j], clue);
					return true;
				}
			}		
		}
	}
	return false;
}

/* function to print a menu */
void print_menu (enum COLOR fg, enum COLOR bg, const char* title, 
					char **items, int num_items, int padding)
{
		/* clear screen */
		printf ("\e[1;1H\e[2J");
		set_color (fg, bg, NORMAL);
		printf ("\u2554");
		for (int i = 0; i < padding; i ++)
			printf ("\u2550");
		printf ("\u2557"); 
		reset_color (); printf ("\n");
		set_color (fg, bg, NORMAL);
		printf ("\u2551");
		set_color (fg, bg, BOLD);
		printf ("%-*s", padding, title);
		reset_color ();
		set_color (fg, bg, NORMAL);
		printf ("\u2551");
		reset_color (); printf ("\n");
		set_color (fg, bg, NORMAL);
		printf ("\u2560");
		for (int i = 0; i < padding; i ++)
			printf ("\u2550");
		printf ("\u2563");
		reset_color (); printf ("\n");
		for (int i = 0; i < num_items; i ++) 
		{
			set_color (fg, bg, NORMAL);
			printf ("\u2551%-*s\u2551", padding, items[i]);
			reset_color (); printf ("\n");
		}
		set_color (fg, bg, NORMAL);
		printf ("\u255A");
		for (int i = 0; i < padding; i ++)
			printf ("\u2550");
		printf ("\u255D");
		reset_color (); printf ("\n");
}

/* reset the player data, loading from the puzzle file */
void reset_player_data (MainPlayerData *app_data, const char *filename)
{
	app_data->puzzle = load_puzzle (filename);

	app_data->is_loaded = app_data->puzzle.grid_frozen;
	app_data->cur_col = -1;
	app_data->cur_row = -1;
	app_data->solution_revealed = false;
	strcpy (app_data->filename, filename);
	/* reset the answer keys */
	for (int i = 0; i < app_data->puzzle.grid_size; i ++)
		for (int j = 0; j < app_data->puzzle.grid_size; j ++)
			app_data->char_ans[i][j] = ' ';
	
}

/* save the user grid to a file */
void save_user_data (MainPlayerData *app_data, const char *filename) 
{
	FILE *outfile;
	outfile = fopen (filename, "wb");
	if (outfile == NULL) 
	{
		fprintf (stderr, ERROR_WRITING_FILE);
		return;
	}
	fprintf (outfile, "%s\n", app_data->filename);
	for (int i = 0; i < app_data->puzzle.grid_size; i ++)
	{
		for (int j = 0; j < app_data->puzzle.grid_size; j ++)
			fprintf (outfile, "%c", app_data->char_ans[i][j]);
		fprintf (outfile, "\n");
	}
	
	fclose (outfile);
}

/* load the user grid from a file */
void load_user_data (MainPlayerData *app_data, const char *filename)
{
	FILE *infile;
	infile = fopen (filename, "rb");
	if (infile == NULL)
	{
		fprintf (stderr, "%s\n", ERROR_READING_FILE);
		return;
	}

	char puzzle_file_name[65535];
	fgets (puzzle_file_name, 65535, infile);	
	reset_player_data (app_data, strtok (puzzle_file_name, "\n"));

	char line[MAX_PUZZLE_SIZE+10];
	for (int i = 0; i < app_data->puzzle.grid_size; i ++)
	{
		fgets (line, MAX_PUZZLE_SIZE+10, infile);
		for (int j = 0; j < app_data->puzzle.grid_size; j ++)
			app_data->char_ans[i][j] = line[j];

	}
	fclose (infile);
}

/* in the player app, move the current selection index left or right */
void move_current_col (MainPlayerData *app_data, enum DIRECTION dir)
{
	int r = app_data->cur_row;
	int c = app_data->cur_col;
	if (dir == DIR_FORWARD)
	{
		c ++;
		while (c < app_data->puzzle.grid_size)
		{
			if (app_data->puzzle.chars[r][c] == '#')
				c ++;
			else
				break;
		}
		if (c < app_data->puzzle.grid_size)
			app_data->cur_col = c;
	}
	else 
	{
		c --;
		while (c >= 0)
		{
			if (app_data->puzzle.chars[r][c] == '#')
				c --;
			else
				break;
		}
		if (c >= 0)
			app_data->cur_col = c;
	}
}

/* in the player app move the current selection index up or down */
void move_current_row (MainPlayerData *app_data, enum DIRECTION dir)
{
	int r = app_data->cur_row;
	int c = app_data->cur_col;
	if (dir == DIR_FORWARD)
	{
		r ++;
		while (r < app_data->puzzle.grid_size)
		{
			if (app_data->puzzle.chars[r][c] == '#')
				r ++;
			else
				break;
		}
		if (r < app_data->puzzle.grid_size)
			app_data->cur_row = r;
	}
	else 
	{
		r --;
		while (r >= 0)
		{
			if (app_data->puzzle.chars[r][c] == '#')
				r --;
			else
				break;
		}
		if (r >= 0)
			app_data->cur_row = r;
	}
}

#endif
