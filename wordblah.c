#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#include "wordblah.h"
#include "constantstrings.h"

/* export the clues to a text file */
void do_export_clues (Puzzle *p)
{
	if (p->grid_frozen == false)
	{
		printf (UNFROZEN_GRID);
		char ch = getchar ();
		return;
	}
	char fname[256];
	printf (INPUT_FILE);
	fgets (fname, 256, stdin);
	if (strlen(fname) == 1)
		return;
	char *filename = strtok (fname, "\n");
	
	export_clues (p, filename);
	printf (FILE_SAVED);
	char ch = getchar ();
}

/* export the puzzle to a png file */
void do_export_puzzle (Puzzle *p)
{
	if (p->grid_frozen == false)
	{
		printf (UNFROZEN_GRID);
		char ch = getchar ();
		return;
	}
	char fname[256];
	printf (INPUT_FILE);
	fgets (fname, 256, stdin);
	if (strlen (fname) == 1)
		return;
	char* filename = strtok (fname, "\n");

	printf (INPUT_EXPORT_ANSWERS);
	char ans[3];
	fgets (ans, 3, stdin);
	bool solution;
	solution = (toupper (ans[0]) == 'Y') ? true : false;
		
	export_grid_image (p, filename, solution);
	printf (FILE_SAVED);
	char ch = getchar ();
}

/* reset the grid */
void do_reset_puzzle (Puzzle *p)
{
	int grid_size = p->grid_size;
	printf (INPUT_CONFIRM_RESET);
	char conf[3];
	fgets (conf, 3, stdin);
	if (toupper (conf[0]) == 'Y')
		init_puzzle (p, grid_size);

	 print_puzzle (p);
	 char ch = getchar ();
}

/* set the solution password for the puzzle */
void do_set_solution_password (Puzzle *p)
{
	char* password;
	password = getpass (INPUT_PASSWORD);
	/* if empty reset the password to nothing */
	if (strlen(password) == 0)
	{
		set_solution_password (p, "\0");
		printf (SOLUTION_PASSWORD_RESET);
		char ch = getchar ();
	}
	/* set the password */
	else 
	{
		set_solution_password (p, (const char* )password);
		printf (PASSWORD_SET);
		char ch = getchar ();
	}
}
/* set the master (editing) password for the puzzle */
void do_set_master_password (Puzzle *p)
{
	char* password;
	password = getpass (INPUT_PASSWORD);
	/* if empty reset the password to nothing */
	if (strlen(password) == 0)
	{
		set_master_password (p, "\0");
		printf (MASTER_PASSWORD_RESET);
		char ch = getchar ();
	}
	/* set the password */
	else 
	{
		set_master_password (p, (const char* )password);
		printf (PASSWORD_SET);
		char ch = getchar ();
	}
}

/* set clue for a word - only for frozen grid */
void do_set_clue_word (Puzzle *p, enum ORIENTATION orient)
{
	print_puzzle (p);
	if (p->grid_frozen == false)
	{
		printf (UNFROZEN_GRID);
		char ch = getchar ();
		return;
	}
	int index;
	String clue; 
	printf (INPUT_INDEX);
	index = get_num ();
	printf (INPUT_CLUE);
	fgets (clue, MAX_CLUE_LENGTH, stdin);
	if (strlen (clue) == 1)
		return;
	char* cl = strtok (clue, "\n");
	
	bool res;
	res = set_clue (p, cl, index, orient);

	if (res == false)
	{
		printf (NO_WORD_INDEX);
		char ch = getchar ();
	}
}

/* clear a cell in the grid */
void do_clear_cell (Puzzle *p)
{
	print_puzzle (p);
	if (p->grid_frozen == true) 
	{
		printf (FROZEN_GRID);
		char ch = getchar ();
		return;
	}
	int row, col;
	printf (INPUT_ROW);
	row = get_num (); 
	printf (INPUT_COL);
	col = get_num ();
	if (row >= p->grid_size || col >= p->grid_size)
	{
		printf (EXCEED_GRID_SIZE);
		char ch = getchar ();
		return;
	}
	p->chars[row][col] = ' ';
	print_puzzle (p);
	
	char ch = getchar ();
}

/* add a down word to the grid */
void do_add_down_word (Puzzle *p)
{
	print_puzzle (p);
	if (p->grid_frozen == true) 
	{
		printf (FROZEN_GRID);
		char ch = getchar ();
		return;
	}
	char wd[MAX_PUZZLE_SIZE];
	int row, col;
	printf (INPUT_WORD);
	fgets (wd, MAX_PUZZLE_SIZE, stdin);
	char *word = is_valid_word (wd);
	if (word == NULL)
	{
		printf (INVALID_WORD);
		char ch = getchar ();
		return;
	}
	printf (INPUT_ROW);
	row = get_num ();
	printf (INPUT_COL);
	col = get_num ();
	if (row >= p->grid_size || col >= p->grid_size) 
	{
		printf (EXCEED_GRID_SIZE);
		char ch = getchar ();
		return;
	}
	if (strlen (word) > (p->grid_size - row))
	{
		printf (WORD_TOO_LONG);
		char ch = getchar ();
		return;
	}
	
	for (int i = row; i < row + strlen(word); i ++)
		p->chars[i][col] = toupper(word[i - row]);
	
	print_puzzle (p);
	char ch = getchar ();
}

/* add an across word to the grid */
void do_add_across_word (Puzzle *p)
{
	print_puzzle (p);
	if (p->grid_frozen == true) 
	{
		printf (FROZEN_GRID);
		char ch = getchar ();
		return;
	}
	char wd[MAX_PUZZLE_SIZE];
	int row, col;	
	printf (INPUT_WORD);
	fgets (wd, MAX_PUZZLE_SIZE, stdin);
	char *word = is_valid_word (wd);
	if (word == NULL)
	{
		printf (INVALID_WORD);
		char ch = getchar ();
		return;
	}
	printf (INPUT_ROW);
	row = get_num ();
	printf (INPUT_COL);
	col = get_num ();
	if (row >= p->grid_size || col >= p->grid_size)
	{
		printf (EXCEED_GRID_SIZE);
		char ch = getchar ();
		return;
	}
	
	if (strlen (word) > (p->grid_size - col))
	{
		printf (WORD_TOO_LONG);
		char ch = getchar ();
		return;
	}
	
	for (int i = col; i < col+strlen (word); i ++)
		p->chars[row][i] = toupper(word[i - col]);
		
	print_puzzle (p);
	char ch = getchar ();
}
/* confirm exit */
bool do_confirm_exit ()
{
	printf (INPUT_CONFIRM_EXIT);
	char res[3];
	fgets (res, 3, stdin);
	if (toupper(res[0]) == 'Y')
		return true;
	else
		return false;
} 

/* main loop for the puzzle editor */
void puzzle_editor_loop (Puzzle *p, const char *filename) 
{
	bool loop = true;
	while (loop) 
	{
		char puzzle_title[60];
		sprintf (puzzle_title, "%s - %s", PUZZLE_MENU_TITLE, filename);
		print_menu (WHITE, BLUE, puzzle_title, PUZZLE_EDIT_MENU, 15, 50);
		printf (INPUT_CHOICE);
		int ch = get_num ();
		switch (ch)
		{
			case 1: print_puzzle (p);
					char ch = getchar ();
					break;
			case 2: do_add_across_word (p);
					break;
			case 3: do_add_down_word (p);
					break;
			case 4: do_clear_cell (p);
					break;
			case 5: freeze_puzzle (p);
					print_puzzle (p);
					ch = getchar ();
					break;
			case 6: unfreeze_puzzle (p);
					print_puzzle (p);
					ch = getchar ();
					break;
			case 7: do_set_clue_word (p, ACROSS);
					break;
			case 8: do_set_clue_word (p, DOWN);
					break;
			case 9: save_puzzle (p, filename);
					printf ("%s\n",FILE_SAVED);
					ch = getchar ();
					break;
			case 10: do_set_master_password (p);
					 break;
			case 11: do_set_solution_password (p);
					 break;
			case 12: do_reset_puzzle (p);
					 break;
			case 13: do_export_puzzle (p);
					 break;
			case 14: do_export_clues (p);
					 break;
			case 15: loop = ! do_confirm_exit ();
					 break;
		}
	}
}

/* open an existing puzzle */
void do_open_puzzle (const char *filename) 
{
	Puzzle p;
	/* if no filename is provided, get it from command line */
	if (filename == NULL) 
	{
		printf (INPUT_FILE);
		char fname[256];
		fgets(fname, 256, stdin);
		if (strlen (fname) == 1)
			return;
		filename = strtok (fname, "\n");
	}

	p = load_puzzle (filename);
	if (p.grid_size == 0)
	{
		printf (INVALID_PUZZLE);
		char ch = getchar ();
		return;
	}
	
	if (strcmp (p.hashed_master_password, "\0") != 0)
	{
		char *passwd;
		passwd = getpass (INPUT_PASSWORD);
		if (strlen (passwd) == 0)
			return;
					
		if (verify_master_password (&p, (const char*) passwd))
			puzzle_editor_loop (&p, filename);
		else
		{
			printf (WRONG_PASSWORD);
			char ch = getchar ();
		} 
	}
	else
		puzzle_editor_loop (&p, filename);
}

/* create a new blank puzzle */
void do_new_puzzle (char *filename, int size) 
{
	Puzzle p; 
	/* if filename is not provided get it from command line */
	if (filename == NULL)
	{
		printf (INPUT_FILE);
		char fname[256];
		fgets (fname, 256, stdin);
		if (strlen (fname) == 1)
			return;
		filename = strtok (fname, "\n"); 
	}
	/* if no size is specified get it from command line */	
	if (size == -1)
	{
		printf (INPUT_GRID_SIZE);
		size = get_num ();
	}
	
	if (size > MAX_PUZZLE_SIZE) 
	{ 
		printf (EXCEED_MAX_GRID_SIZE);
		char c = getchar (); 
	}
	else
	{
		init_puzzle (&p, size);
		puzzle_editor_loop (&p, filename);
	}
}

/* The main loop of the program */
int main_loop () 
{
	/* Print the main menu */
	while (1)
	{
		print_menu (WHITE, BLUE, MAIN_MENU_TITLE, MAIN_MENU, 3, 50);
		printf (INPUT_CHOICE);
		int ch = get_num ();
		switch (ch)
		{
			case 1: do_new_puzzle (NULL, -1);
					break;
			case 2: do_open_puzzle (NULL);
					break;
			case 3: exit (0);
		}
	}
}

int main (int argc, char* argv[]) 
{
	if (argc >= 2)
	{
		Puzzle p;
		switch (argc)
		{
			case 2 : do_open_puzzle (argv[1]);
					 break;
			case 4 : if (strcmp (argv[2], "new") == 0)
					 {
					 	int grid_size = atoi (argv[3]);
						do_new_puzzle (argv[1], grid_size);
					 	break;
					 }
			default: fprintf (stderr, USAGE_LINE_1, argv[0]);
					 fprintf (stderr, USAGE_LINE_2);
					 fprintf (stderr, USAGE_LINE_3);
					 exit (3);
		}
	}
	return (main_loop ());
}
