#ifndef __CONSTANTSTRINGS_H
#define __CONSTANTSTRINGS_H

#define VERSION  "0.1a"
#define LICENSE_BSD "Licensed under the BSD 3-clause license \
https://opensource.org/licenses/BSD-3-Clause"
#define FROZEN_GRID "Grid is frozen. Unfreeze grid first!"
#define UNFROZEN_GRID "Grid is unfrozen. Freeze grid first!"
#define EXCEED_GRID_SIZE "Row/col exceeded grid size!"
#define INPUT_ROW "Enter the row: "
#define INPUT_COL "Enter the col: "
#define INPUT_INDEX "Enter the word index: "
#define INPUT_CLUE "Enter the clue: "
#define WORD_TOO_LONG "Word too long"
#define INPUT_WORD "Enter the word: "
#define INPUT_FILE "Enter the file name: "
#define PUZZLE_MENU_TITLE "Edit Puzzle"
#define MAIN_MENU_TITLE "Main Menu"
#define INPUT_GRID_SIZE "Number of rows/columns: "
#define INPUT_PASSWORD "Enter the password: "
#define INPUT_EXPORT_ANSWERS "Export as solution (y/N): "
#define INPUT_CHOICE "Your Choice: "
#define EXCEED_MAX_GRID_SIZE "Exceeds max puzzle size"
#define ERROR_WRITING_FILE "Error writing file"
#define ERROR_READING_FILE "Error reading file"
#define COMPRESSED " (compressed)"
#define INVALID_WORD "Word contains illegal characters. Only alphabets allowed!"
#define INVALID_PUZZLE "Puzzle is an invalid wordblah file or corrupted!"
#define FILE_SAVED "File saved successfully"
#define PASSWORD_SET "Password set successfully"
#define MASTER_PASSWORD_RESET "Master password reset successfully. Puzzle no \
longer password protected!"
#define SOLUTION_PASSWORD_RESET "Solution password reset successfully. Solution\
 can be viewed in the player without a password!"
#define WRONG_PASSWORD "Wrong password!"
#define NO_WORD_INDEX "No such word with specified index" 
#define INPUT_CONFIRM_EXIT "Are you sure you wish to exit? \
Unsaved changes will be lost [y/N]"
#define INPUT_CONFIRM_RESET "Are you sure? This will destroy the entire grid\
and is irreversible (y/N): "
#define ACROSS_CLUES "ACROSS - CLUES"
#define DOWN_CLUES "DOWN - CLUES"

#define USAGE_LINE_1 "Usage: %s [<filename> [new <nn>]]\n"
#define USAGE_LINE_2 "<filename> - puzzle file name\n"
#define USAGE_LINE_3 "new <nn> - create new puzzle with <nn> grid \
columns (warning: existing file name may be overwritten)\n"

/* for wordblah_player */
#define ERROR_ICON "Unable to load icon!"
#define ERROR_WINDOW "Error loading Window Resource!"
#define OPEN_FILE "Open File"
#define SAVE_FILE "Save File"
#define UNFROZEN_GRID_PLAYER "Puzzle is not finalized/frozen and hence cannot\
 be played"

/* about box info */
const char *AUTHOR[] = { "V. Harishankar", NULL};
const char *COPYRIGHT = "Copyright 2020 V.Harishankar";
const char *COMMENTS = "A player for wordblah (crossword puzzles)";
const char *PROGRAM_NAME = "Wordblah Player";
const char *WEBSITE = "http://harishankar.org/";
const char *WEBSITE_LABEL = "Author's HomePage";

char *MAIN_MENU[] = 
					{"1. New puzzle",
					 "2. Open existing puzzle", 
					 "3. Exit"};

char *PUZZLE_EDIT_MENU[] = 
					{ "1. Display grid", 
					"2. Add word across", 
					"3. Add word down", 
					"4. Clear Cell", 
					"5. Freeze grid", 
					"6. Unfreeze grid",
					"7. Set Clue - Across Word",
					"8. Set Clue - Down Word",
					"9. Save puzzle",
					"10.Set master (editing) password",
					"11.Set solution password",
					"12.Reset entire grid",
					"13.Export puzzle as PNG image",
					"14.Export clues to text file",
					"15.Return to main menu" };

#endif
